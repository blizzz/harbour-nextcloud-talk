#ifndef DISCOVERYRUN_H
#define DISCOVERYRUN_H

#include <QString>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include "providers/qnamprovider.h"

class DiscoveryRun : public QObject
{
    Q_OBJECT
public:
    explicit DiscoveryRun(QString url, QObject *parent = nullptr);
    explicit DiscoveryRun(QString url, QString loginName, QString token, QObject *parent = nullptr);
    enum DiscoveryResult {
        Available = 0,
        OtherError = 1,
        HostNotFound = 2,
        Redirect = 3,
    };

public slots:
    void checkAvailability();
    void verifyCredentials();
    void availabilityCheckFinished();
    void credentialsCheckFinished();

signals:
    void protocolUnsupported(QString originalHost);
    void nextcloudDiscoveryFinished(int result, QUrl host, QString originalHost);
    void verifyCredentialsFinished(bool isVerified, QString host, QString originalUrl, QString loginName, QString token, QString userId);

private slots:
    void testCredentials(int result, QUrl host, QString originalHost);

private:
    QString originalUrl;
    QUrl nc_server;
    QString m_loginName;
    QString m_token;
    QNetworkAccessManager* m_nam = QNAMProvider::getInstance();
};

#endif // DISCOVERYRUN_H
