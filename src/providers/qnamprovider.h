#ifndef QNAMPROVIDER_H
#define QNAMPROVIDER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkDiskCache>
#include <QStandardPaths>


class QNAMProvider : public QObject
{
    Q_OBJECT
public:
    static QNetworkAccessManager* getInstance() {
        static auto instance = QNAMProvider::buildInstance();
        return instance.data();
    }
    static QNetworkAccessManager* getInstance(const Qt::HANDLE threadId) {
        auto key = QString("0x%1").arg((intptr_t)threadId, 16);

        static QMap<QString, QSharedPointer<QNetworkAccessManager>> map;
        if (!map.contains(key)) {
            map.insert(key, QNAMProvider::buildInstance());
        }
        return map.value(key).data();
    }

private:
    static QSharedPointer<QNetworkAccessManager> buildInstance() {
        auto instance = QSharedPointer<QNetworkAccessManager>(new QNetworkAccessManager(), &QObject::deleteLater);
        auto diskCache = new QNetworkDiskCache(instance.data());
        diskCache->setCacheDirectory(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
        instance.data()->setCache(diskCache);
        return instance;
    }

};

#endif // QNAMPROVIDER_H
