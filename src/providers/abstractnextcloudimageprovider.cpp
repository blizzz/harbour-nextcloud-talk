#include "abstractnextcloudimageprovider.h"

#include <QException>
#include <QNetworkReply>
#include "../services/accounts.h"
#include "qnamprovider.h"

class AsyncImageResponse : public QQuickImageResponse
{
    public:
        AsyncImageResponse(QNetworkRequest request)
        {
            m_reply = m_qnam->get(request);
            connect(m_reply, &QNetworkReply::finished, this, &AsyncImageResponse::imageReceived);
        }

        void imageReceived() {
            m_reply->deleteLater();
            m_image.loadFromData(m_reply->readAll());
            if (m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute) != 200) {
                m_error = "Network error: " + m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString();
            }
            emit finished();
        }

        virtual QString errorString() const override {
            return m_error;
        }

        QQuickTextureFactory *textureFactory() const override
        {
            return QQuickTextureFactory::textureFactoryForImage(m_image);
        }

        QImage m_image;
        QString m_error = "";
        QNetworkReply *m_reply;
    private:
        QNetworkAccessManager* m_qnam = QNAMProvider::getInstance(QThread::currentThreadId());
        Accounts* m_accountService = Accounts::getInstance();
};

QQuickImageResponse *AbstractNextcloudImageProvider::requestImageResponse(const QString &id, const QSize &requestedSize)
{
    NextcloudAccount* account = nullptr;
    try {
        account = accountFromId(id);
    } catch (...) {
        return nullptr;
    }
    QString subject = id;
    subject.remove(0, id.indexOf('/') + 1);
    QNetworkRequest request = getRequest(subject, account, requestedSize);
    AsyncImageResponse *response = new AsyncImageResponse(request);
    return response;
}

NextcloudAccount* AbstractNextcloudImageProvider::accountFromId(const QString &id)
{
    const int accountId = id.left(id.indexOf('/')).toInt();
    Accounts* accountService = Accounts::getInstance();
    NextcloudAccount* account = accountService->getAccountById(accountId);
    return account;
}
