#ifndef UPLOAD_H
#define UPLOAD_H

#include <QFile>
#include <QObject>
#include <QQmlEngine>
#include <QVector>
#include "../providers/qnamprovider.h"

class Upload : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Upload)
    Upload() {
        connect(this, SIGNAL(stageTarget(QString)), SLOT(findTargetPathName(QString)));
        connect(this, SIGNAL(stageUpload(QString)), SLOT(startUpload(QString)));
        connect(this, SIGNAL(stageMessage(QString)), SLOT(postMessage(QString)));
        connect(this, SIGNAL(stageCreateUploadFolder(QString)), SLOT(createUploadFolder(QString)));
    }
public:
    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
    {
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)

        return new Upload;
    }

    struct UploadRequest {
        UploadRequest() {}

        QString id;
        QString sourcePath;
        QString targetPath;
        uint8_t targetIndex = 1;
        QString room;
        QSharedPointer<QFile> file;
        int accountId;

        bool operator==(const UploadRequest &toCompare) const {
            return id == toCompare.id;
        }
    };

    const int32_t ERROR_ATTACHMENT_NOT_ALLOWED = -1;

public slots:
    QString uploadFile(QString path, QString room, int accountId);

signals:
    void uploadStarted(QString transferId);
    void uploadCompleted(QString transferId);
    void uploadFailed(QString transferId, int errorCode);
    void uploadProgress(QString transferId, int relProgress);

    // privately used signals
    void stageTarget(QString transferId, QPrivateSignal);
    void stageUpload(QString transferId, QPrivateSignal);
    void stageMessage(QString transferId, QPrivateSignal);
    void stageCreateUploadFolder(QString transferId, QPrivateSignal);

private slots:
    void findTargetPathName(QString transferId);
    void startUpload(QString transferId);
    void postMessage(QString transferId);
    void requestFinished();
    void onUploadProgress(QString transferId, qint64 sent, qint64 total);
    void createUploadFolder(QString transferId);
    QString composeFinalFilename(UploadRequest *ur) const;

private:
    QVector<UploadRequest> m_pendingRequests;
    QNetworkAccessManager* m_nam = QNAMProvider::getInstance();
    UploadRequest* getUploadRequestById(QString id);

};

#endif // UPLOAD_H
