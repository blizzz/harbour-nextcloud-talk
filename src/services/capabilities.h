#ifndef CAPABILITIES_H
#define CAPABILITIES_H

#include "../nextcloudaccount.h"
#include <QColor>
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonObject>
#include "../providers/qnamprovider.h"

class Capabilities : public QObject
{
    Q_OBJECT
public:
    Capabilities(NextcloudAccount *account);
    bool areAvailable() const;
    void request();
    NextcloudAccount *m_account;
    int getConversationApiLevel() const;
    QColor primaryColor() const;
    QUrl logoUrl() const;
    QString name() const;
    bool attachmentsAllowed() const;
    QString attachmentsFolder() const;

private:
    QNetworkAccessManager* m_nam = QNAMProvider::getInstance();
    bool m_available = false;
    bool m_isRequestRunning = false;
    QJsonObject m_capabilities;
    QNetworkReply *m_reply = nullptr;
    QByteArray m_talkCapHash;

public slots:
    void requestFinished();
    void handleError(QNetworkReply::NetworkError);
    void checkTalkCapHash();
};

#endif // CAPABILITIES_H
