#ifndef ACCOUNTS_H
#define ACCOUNTS_H

#include <QAbstractListModel>
#include <QStringList>
#include <QVector>
#include "../nextcloudaccount.h"
#include "secrets.h"

class Accounts : public QAbstractListModel
{
    Q_OBJECT
public:
    enum AccountRoles {
        NameRole = Qt::UserRole + 1,
        AccountRole = Qt::UserRole + 2,
        LogoRole = Qt::UserRole + 3,
        InstanceNameRole = Qt::UserRole + 4,
        ColorRole = Qt::UserRole + 5,
        ColorModeRole = Qt::UserRole + 6,
        AccountEnabledRole = Qt::UserRole + 7,
    };
    enum ColorMode {
        InstanceColor,
        OverriddenColor,
    };
    Q_ENUM(AccountRoles)
    Q_ENUM(ColorMode)
    explicit Accounts(QObject *parent = nullptr);
    static Accounts* getInstance();
    NextcloudAccount* getAccountById(const int id);
    QVector<NextcloudAccount*> getAccounts();

    // Basic functionality:
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    QHash<int, QByteArray> roleNames() const;

public slots:
    void addAccount(QString url, QString loginName, QString token, QString userId);
    void deleteAccount(int accountId);
    void loadAccounts();

private:
    Accounts(const Accounts*);
    QVector<NextcloudAccount*> readAccounts();
    QVector<NextcloudAccount*> m_accounts;
    QString getSettingsPath();
    bool is_initialized = false;
    int max_id = 0;
    Secrets m_secrets;

};

#endif // ACCOUNTS_H
