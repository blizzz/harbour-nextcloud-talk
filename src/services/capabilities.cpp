#include "capabilities.h"
#include "requestfactory.h"

#include <QMetaMethod>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

Capabilities::Capabilities(NextcloudAccount *account) {
    m_account = account;
}

bool Capabilities::areAvailable() const {
    return m_available;
}

int Capabilities::getConversationApiLevel() const
{
    if (m_capabilities
            .find("spreed").value().toObject()
            .find("features").value().toArray()
            .toVariantList().contains("conversation-v4"))
    {
        return 4;
    }

    if (m_capabilities
            .find("spreed").value().toObject()
            .find("features").value().toArray()
            .toVariantList().contains("conversation-v2"))
    {
        return 2;
    }

    return 1;
}

QColor Capabilities::primaryColor() const {
    QColor color;
    if(!m_available) {
        qDebug() << "capabilities have not been requested yet!";
        return color;
    }

    color.setNamedColor(m_capabilities
                        .find("theming").value().toObject()
                        .find("color").value().toString());

    if (!color.isValid())
    {
        // fallback to Nextcloud-blue when no color is provided
        color.setNamedColor("#0082c9");
    }

    return color;
}

QUrl Capabilities::logoUrl() const {
    if(!m_available) {
        qDebug() << "capabilities have not been requested yet!";
        return QUrl();
    }

    return QUrl::fromUserInput(m_capabilities
                               .find("theming").value().toObject()
                               .find("logo").value().toString()
                               );
}

QString Capabilities::name() const {
    if(!m_available) {
        qDebug() << "capabilities have not been requested yet!";
        return "";
    }

    return m_capabilities
            .find("theming").value().toObject()
            .find("name").value().toString();
}

bool Capabilities::attachmentsAllowed() const
{
    if(!m_available) {
        qDebug() << "capabilities have not been requested yet!";
        return false;
    }

    return m_capabilities
            .find("spreed").value().toObject()
            .find("config").value().toObject()
            .find("attachments").value().toObject()
            .keys().contains("allowed");
}

QString Capabilities::attachmentsFolder() const
{
    static const QString defaultFolder = "/Talk";

    if(!m_available) {
        qDebug() << "capabilities have not been requested yet – using default upload folder!";
        return defaultFolder;
    }

    const QJsonObject attachmentCaps = m_capabilities
            .find("spreed").value().toObject()
            .find("config").value().toObject()
            .find("attachments").value().toObject();

    return attachmentCaps.contains("folder") ? attachmentCaps.find("folder").value().toString() : defaultFolder;
}

void Capabilities::request() {
    qDebug("Fetching capabilities invoked");
    if(m_isRequestRunning) {
        qDebug("Request for capabilities is already running");
        return;
    }


    QUrl endpoint = QUrl(m_account->host());
    endpoint.setQuery("format=json");
    endpoint.setPath(endpoint.path() + "/ocs/v2.php/cloud/capabilities");

    QNetworkRequest request = RequestFactory::getRequest(endpoint, m_account);
    m_reply = m_nam->get(request);
    connect(m_reply, &QNetworkReply::finished, this, &Capabilities::requestFinished);
    connect(m_reply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), this, &Capabilities::handleError);
    m_isRequestRunning = true;
}

void Capabilities::requestFinished() {
    m_reply->deleteLater();

    if(m_reply->error() != QNetworkReply::NoError
            || m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() != 200)
    {
        qDebug() << "network issue or unauthed, code" << m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt()
                 << m_reply->errorString();
        if(m_nam->networkAccessible() != QNetworkAccessManager::Accessible) {
            qDebug() << "Network not accessible";
        }
        return;
    }

    QByteArray payload = m_reply->readAll();
    QJsonDocument apiResult = QJsonDocument::fromJson(payload);
    QJsonObject q = apiResult.object();
    QJsonObject root = q.find("ocs").value().toObject();
    QJsonObject data = root.find("data").value().toObject();
    m_capabilities = data.find("capabilities").value().toObject();
    m_available = true;
    m_isRequestRunning = false;
}

void Capabilities::handleError(QNetworkReply::NetworkError error) {
    m_reply->deleteLater();
    m_isRequestRunning = false;
    qDebug() << "Capabilities Network Error:" << m_reply->errorString() << error;
}

void Capabilities::checkTalkCapHash() {
    // called from external, do not deleteLater reply here
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if(reply->property("AccountID") != this->m_account->id()) {
        return;
    }
    QByteArray newHash = reply->rawHeader("X-Nextcloud-Talk-Hash");
    if (m_talkCapHash == newHash || newHash == "") {
        return;
    } else if (m_talkCapHash == "") {
        m_talkCapHash = newHash;
        return;
    }
    m_available = false;
    m_talkCapHash = newHash;
    this->request();
}
