#include "upload.h"

#include <cmath>
#include <QBuffer>
#include <QCryptographicHash>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include "accounts.h"
#include "capabilities.h"
#include "requestfactory.h"
#include "../helpers/stringhelper.cpp"


#include <QDebug>


QString Upload::uploadFile(QString path, QString room, int accountId)
{
    Accounts* accountService = Accounts::getInstance();
    QString unhashedId = path + ">" + room + ">" + QString::number(accountId) + ">" + QString::number(qrand());
    // only 40chars as some Talk versions cut them at this length
    QString transferId = QString(QCryptographicHash::hash(unhashedId.toUtf8(), QCryptographicHash::Sha3_256).toHex()).left(40);

    emit uploadStarted(transferId);

    NextcloudAccount* account = accountService->getAccountById(accountId);

    if (!account->capabilities()->attachmentsAllowed()) {
        emit uploadFailed(transferId, ERROR_ATTACHMENT_NOT_ALLOWED);
        return "";
    }

    UploadRequest ur;
    ur.accountId = accountId;
    ur.id = transferId;
    ur.room = room;
    ur.sourcePath = path;
    ur.targetPath = account->capabilities()->attachmentsFolder();
    NCT::Helpers::Strings::ltrim(&ur.targetPath, "/");

    m_pendingRequests.append(ur);

    QPrivateSignal p;
    emit stageTarget(transferId, p);
    return transferId;
}

void Upload::findTargetPathName(QString transferId)
{
    qDebug() << "determining target file name for tId" << transferId;

    UploadRequest* ur = getUploadRequestById(transferId);

    Accounts* accountService = Accounts::getInstance();
    NextcloudAccount* account = accountService->getAccountById(ur->accountId);

    QString filename = composeFinalFilename(ur);
    QUrl endpoint = QUrl(account->host());
    endpoint.setPath(endpoint.path() + "/remote.php/webdav/" + ur->targetPath + "/" + filename);

    auto *buf = new QBuffer(this);
        buf->setData("");
        buf->open(QIODevice::ReadOnly);

    QNetworkRequest request = RequestFactory::getRequest(endpoint, account);
    QNetworkReply *reply = m_nam->sendCustomRequest(request, "PROPFIND", buf);
    reply->setProperty("transferId", transferId);
    reply->setProperty("stage", "findTargetName");
    reply->setProperty("attempt", 0);
    connect(reply, &QNetworkReply::finished, this, &Upload::requestFinished);
}

void Upload::startUpload(QString transferId)
{
    qDebug() << "uploading file, transfer id" << transferId;

    UploadRequest* ur = getUploadRequestById(transferId);

    Accounts* accountService = Accounts::getInstance();
    NextcloudAccount* account = accountService->getAccountById(ur->accountId);

    QString filename = composeFinalFilename(ur);
    QUrl endpoint = QUrl(account->host());
    endpoint.setPath(endpoint.path() + "/remote.php/webdav/" +  ur->targetPath + "/" + filename);

    ur->file = QSharedPointer<QFile>(new QFile(ur->sourcePath), &QObject::deleteLater);
    ur->file.data()->open(QIODevice::ReadOnly);

    QNetworkRequest request = RequestFactory::getRequest(endpoint, account);
    QNetworkReply *reply = m_nam->put(request, ur->file.data());
    reply->setProperty("transferId", transferId);
    reply->setProperty("stage", "fileUpload");
    connect(reply, &QNetworkReply::uploadProgress, [=](qint64 sent, qint64 total){
        onUploadProgress(transferId, sent, total);
    });
    connect(reply, &QNetworkReply::finished, this, &Upload::requestFinished);
}

void Upload::postMessage(QString transferId)
{
    qDebug() << "upload done, now post to Talk, transfer id" << transferId;

    UploadRequest* ur = getUploadRequestById(transferId);

    Accounts* accountService = Accounts::getInstance();
    NextcloudAccount* account = accountService->getAccountById(ur->accountId);

    QString filename = composeFinalFilename(ur);
    QUrl endpoint = QUrl(account->host());
    endpoint.setPath(endpoint.path() + "/ocs/v2.php/apps/files_sharing/api/v1/shares");

    QNetworkRequest request = RequestFactory::getRequest(endpoint, account);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json;charset=utf-8");

    QJsonObject parameters;
    parameters.insert("path", QJsonValue("/" + ur->targetPath + "/" + filename));
    parameters.insert("shareType", QJsonValue(10));
    parameters.insert("shareWith", QJsonValue(ur->room));
    parameters.insert("referenceId", QJsonValue(ur->id.left(40))); // bug in Talk will shorten it to 40, align with logic in QML
    QJsonDocument payload = QJsonDocument(parameters);

    QNetworkReply *reply = m_nam->post(request, payload.toJson());
    reply->setProperty("transferId", transferId);
    reply->setProperty("stage", "shareToRoom");
    connect(reply, &QNetworkReply::finished, this, &Upload::requestFinished);
}

void Upload::createUploadFolder(QString transferId)
{
    qDebug() << "upload folder missing, now create it, transfer id" << transferId;

    UploadRequest* ur = getUploadRequestById(transferId);

    Accounts* accountService = Accounts::getInstance();
    NextcloudAccount* account = accountService->getAccountById(ur->accountId);

    QUrl endpoint = QUrl(account->host());
    endpoint.setPath(endpoint.path() + "/remote.php/webdav/" + ur->targetPath);

    auto *buf = new QBuffer(this);
        buf->setData("");
        buf->open(QIODevice::ReadOnly);

    // does not work recursively (edge case) – folder is created when choosing on Nextcloud anyway
    QNetworkRequest request = RequestFactory::getRequest(endpoint, account);
    QNetworkReply *reply = m_nam->sendCustomRequest(request, "MKCOL", buf);
    reply->setProperty("transferId", transferId);
    reply->setProperty("stage", "createUploadFolder");
    connect(reply, &QNetworkReply::finished, this, &Upload::requestFinished);
}

void Upload::requestFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    reply->deleteLater();

    qDebug() << "upload request finished, stage:" << reply->property("stage");
    QVariant tid = reply->property("transferId");
    if (!tid.isValid()) {
        qDebug() << "request was not having a transfer id";
        return;
    }
    UploadRequest* ur = getUploadRequestById(tid.toByteArray());
    QPrivateSignal p;

    if(reply->property("stage") == "findTargetName") {
        if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 404) {
            emit stageUpload(ur->id, p);
        } else if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 207) {
            ur->targetIndex++;
            findTargetPathName(ur->id);
        } else {
            emit uploadFailed(ur->id, reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt());
        }
    } else if(reply->property("stage") == "fileUpload") {
        ur->file.data()->close();
        if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 201) {
            emit stageMessage(ur->id, p);
            return;
        } else if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 409) {
            // the target folder might not exist, try to create and retry upload
            emit stageCreateUploadFolder(ur->id, p);
            return;
        }
        qDebug() << "Upload failed:" << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        qDebug() << "Qt Ne Error:" << reply->error();
        emit uploadFailed(ur->id, reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt());
    } else if(reply->property("stage") == "shareToRoom") {
        if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 200) {
            emit uploadCompleted(ur->id);
            return;
        }
        qDebug() << "Upload failed:" << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        qDebug() << "Qt Ne Error:" << reply->error();
        emit uploadFailed(ur->id, reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt());
    } else if(reply->property("stage") == "createUploadFolder") {
        if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 201) {
            emit stageUpload(ur->id, p);
            return;
        }
        qDebug() << "Upload failed:" << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        qDebug() << "Qt Ne Error:" << reply->error();
        emit uploadFailed(ur->id, reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt());
    }
}

void Upload::onUploadProgress(QString transferId, qint64 sent, qint64 total)
{
    // the final call will have values 0, 0
    if (total > 0) {
        uint16_t percentage = static_cast<uint16_t>(round(static_cast<double>(100 * sent / total)));
        emit uploadProgress(transferId, percentage);
    }
}

Upload::UploadRequest* Upload::getUploadRequestById(QString id)
{
    UploadRequest r;
    r.id = id;
    return &m_pendingRequests[m_pendingRequests.indexOf(r)];
}

QString Upload::composeFinalFilename(UploadRequest *ur) const
{
    auto fileinfo = QFileInfo(ur->sourcePath);
    QString filename = fileinfo.fileName();
    if (ur->targetIndex > 1) {
        QString counter = " (" + QString::number(ur->targetIndex) + ")";
        int lastDotPos = filename.lastIndexOf('.');
        if (lastDotPos == -1) {
            filename += counter;
        } else {
            filename.insert(lastDotPos, counter);
        }
    }
    return filename;
}
