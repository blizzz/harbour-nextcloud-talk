<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AccountSettings</name>
    <message>
        <source>from instance theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>selected by you</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Accounts</name>
    <message>
        <source>Accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enable account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LegacyAddAccount</name>
    <message>
        <source>Add account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter a Nextcloud host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verify Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter login name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter app password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verify Credentials</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NcAuthorLine</name>
    <message>
        <source>Pending…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NcChatMessage</name>
    <message>
        <source>shared</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PendingUpload</name>
    <message>
        <source>Upload failed. No automatic retry.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>participants</name>
    <message>
        <source>Participants</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>moderator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>away</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>do not disturb</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>room</name>
    <message>
        <source>Reply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mention</source>
        <translation></translation>
    </message>
    <message>
        <source>You are sharing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>rooms</name>
    <message>
        <source>Accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Last update: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
