import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import harbour.nextcloud.talk 1.0
import "../../components"
import "../../components/Chat"
import "../../lib/NcChatTools.js" as ChatTools

Page {
    id: room
    allowedOrientations: Orientation.All

    property string token;
    property string roomName;
    property int accountId;
    property string accountUserId;
    property bool hasAttachments: undefined;
    property int replyToId: -1;
    property string replyToMsg: "";
    property var runningUploads: ({});
    property var pendingMessages: ({});
    property bool preventClearing: false;

    onStatusChanged: {
        if(status === PageStatus.Activating) {
            if(!roomService.isPolling(token, accountId)) {
                // do not re-enable when returning from participants
                roomService.startPolling(token, accountId)
            }
            preventClearing = false;
        } else if(status === PageStatus.Deactivating) {
            if(pageStack.currentPage.pageName !== "Participants" && !preventClearing) {
                roomService.stopPolling()
            }
        } else if(status === PageStatus.Inactive) {
            if(pageStack.currentPage.pageName !== "Participants" && !preventClearing) {
                messages.clear();
            }

            pageStack.popAttached()
        } else if(status === PageStatus.Active) {
            pageStack.pushAttached(Qt.resolvedUrl("./participants.qml"),
                {
                    token: room.token,
                    accountId: room.accountId,
                    textField: sendMessage
                }
            );
        }
    }

    function prepareMessage(message) {
        message._lastOfActorGroup = true
        message._firstOfActorGroup = true
        message._isPending = false
        message._mid = message.id

        message._type = "posting"
        if(message.message === "{file}") {
            message._type = "file"
        }

        message.rawText = message.message

        message.repliedTo = {
            author: "",
            message: ""
        }

        if(message.parent) {
            if(message.parent.messageParameters) {
                message.parent.message = ChatTools.handleMessageParameters(
                    message.parent.messageParameters,
                    message.parent.message,
                    room.accountUserId,
                    room.token
                );
            }
            message.repliedTo.author = message.parent.actorDisplayName
            message.repliedTo.message = stripTags(message.parent.message)
        }

        return message
    }

    function filterParameters(message) {
        message.filteredParameters = {}

        Object.keys(message.messageParameters).forEach(function(key) {
            message.filteredParameters[key] = {
                type: message.messageParameters[key].type,
                id: message.messageParameters[key].id,
                name: message.messageParameters[key].name
            }

            switch(message.messageParameters[key].type) {
                case "file":
                    message.filteredParameters[key].path = message.messageParameters[key].path
                    message.filteredParameters[key].link = message.messageParameters[key].link
                    message.filteredParameters[key].mimeType = message.messageParameters[key].mimetype
                    message.filteredParameters[key].previewAvailable = message.messageParameters[key]['preview-available'] === "yes"
                    break
            }

        })

        delete message.messageParameters // Otherwise QML runs into type problems sometimes (VariantMap vs List) in next step
        return message
    }

    function stripTags(s) {
        return s.replace(/(<([^>]+)>)/ig,"")
    }

    function __linkReplacer(_, leadingSpace, protocol, url, trailingSpace) {
        var linkText = url
        if (!protocol) {
            protocol = 'https://'
        } else if (protocol === 'http://') {
            linkText = protocol + url
        }

        return leadingSpace
                + '<a rel="noopener noreferrer" href="'
                + protocol
                + url
                + '">'
                + linkText
                + '</a>'
                + trailingSpace
    }

    function updateLastOfActor(message) {
        if(messages.count > 0) {
            var previousMessage = messages.get(messages.count - 1)
            if(previousMessage && previousMessage.actorId === message.actorId) {
                previousMessage._lastOfActorGroup = false
            }
        }
    }

    function updateFirstOfActor(message) {
        if(messages.count > 0) {
            var previousMessage = messages.get(messages.count - 1)
            if(previousMessage && previousMessage.actorId === message.actorId) {
                message._firstOfActorGroup = false
            }
        }
    }

    function appendPendingUploadMessage(message) {
        messages.append(message)
        console.debug("saving reference ID " + message.referenceId)
        room.pendingMessages[message.referenceId] = messages.count - 1
        console.debug(Object.keys(room.pendingMessages))
    }

    function prepareUploadMessage(uploadName, uploadPath, mimeType, referenceId) {
        var message = {}
        message.actorId = accountUserId
        message.actorDisplayName = ""
        message._type = "upload"
        message.filteredParameters = {
            file: {
                id: "",
                name: uploadName,
                path: uploadPath,
                mimeType: mimeType
            }
        }

        message.referenceId = referenceId

        message.repliedTo = {
            author: "",
            message: ""
        }

        message.isReplyable = false
        message.actorType = "user"
        message._lastOfActorGroup = false
        message._firstOfActorGroup = true
        message._isPending = true

        var actorSnippet = ChatTools.createMentionSnippet(
            {
                id: accountUserId,
                name: qsTr("You")
            },
            accountUserId,
            token
        );

        message.rawText = message.message = qsTr("You are sharing") + " " + uploadName;

        return message
    }

    SilicaListView {
        id: chat
        anchors {
            top: parent.top
            bottom: sendMessagePart.top
            left: parent.left
            right: parent.right
            leftMargin: Theme.horizontalPageMargin
            rightMargin: Theme.horizontalPageMargin
        }
        width: parent.width
        height: parent.height - sendMessage.height
        contentHeight: height
        boundsBehavior: Flickable.DragOverBounds
        quickScroll: true
        quickScrollAnimating: true
        clip: true

        header: PageHeader {
            id: header
            title: roomName
        }
        headerPositioning: ListView.PullBackHeader

        delegate: ListItem {
            id: messageItem

            contentHeight: author.contentHeight
                    + repliedToWrapper.height
                    + Math.max(messageText.height, avatar.height)
                    + Theme.paddingLarge
                    + filePreview.height

            // without the specified height, a long press on the file preview would have the
            // context menu been laid over the preview
            height: contentHeight + ctxMenu.height

            Row {
                spacing: Theme.paddingSmall

                Avatar {
                    id: avatar
                    account: accountId
                    user: actorId
                    anchors.bottom: parent.bottom
                    opacity: _lastOfActorGroup ? 100 : 0
                }

                Column {
                    width: chat.width - avatar.width - Theme.paddingSmall
                    spacing: Theme.paddingSmall

                    NcAuthorLine {
                        id: author
                        isPending: _isPending
                        firstActorOfGroup: _firstOfActorGroup
                        time: timestamp
                        author: actorDisplayName
                    }

                    NcRepliedToMessage {
                        id: repliedToWrapper
                        width: chat.width - avatar.width - Theme.paddingSmall
                        author: repliedTo.author
                        message: repliedTo.message
                    }

                    NcChatMessage {
                        id: messageText
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        rawMessage: rawText
                        userId: room.accountUserId
                        roomId: room.token
                        parameters: filteredParameters
                        accountId: room.accountId
                    }

                    Loader {
                        id: filePreview
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        height: this.item ? this.item.size : 0

                        MouseArea {
                            anchors.fill: parent;
                            propagateComposedEvents: true
                            onPressAndHold: {
                                if (_type === "file") {
                                    ctxMenu.open(messageItem)
                                }
                            }
                        }

                        source: {
                            switch(_type) {
                                case "file":
                                    return "../../components/FilePreview.qml"
                                case "upload":
                                    return "../../components/PendingUpload.qml"
                                default:
                                    return ""
                            }
                        }

                        onItemChanged: {
                            this.item.filePath = filteredParameters.file.path
                            this.item.size = Math.round(Theme.itemSizeHuge * 1.25)

                            switch(_type) {
                                case "file":
                                    this.item.fileId = filteredParameters.file.id
                                    this.item.account = accountId
                                    this.item.mimeType = filteredParameters.file.mimeType
                                    this.item.previewAvailable = filteredParameters.file.previewAvailable
                                    if (!this.item.previewAvailable) {
                                        this.item.size = Theme.itemSizeMedium
                                    }
                                    return
                                case "upload":
                                    this.item.transferReference = referenceId
                                    this.item.isImage = filteredParameters.file.mimeType.indexOf("image/") === 0
                                    if (this.item.isImage === false) {
                                        this.item.size = Math.floor(Theme.itemSizeExtraSmall / 2)
                                    }
                                    return
                                default:
                                    return
                            }
                        }
                    }
                }
            }

            ListView.onRemove: RemoveAnimation {
                target: messageItem
            }

            menu: ContextMenu {
                id: ctxMenu;
                container: chat
                MenuItem {
                    text: qsTr("Reply")
                    visible: isReplyable ? true : false
                    onClicked: {
                        replyToId = _mid
                        replyToMsg = stripTags(message)
                        sendMessage.focus = true
                    }
                }
                MenuItem {
                    text: qsTr("Mention")
                    visible: actorType === "users"
                    onClicked: {
                        sendMessage.text = sendMessage.text + " @" + actorId;
                    }
                }
                MenuItem {
                    text: qsTr("Copy text")
                    onClicked: Clipboard.text = stripTags(message)
                }
            }
        }

        model: ListModel {
            id: messages
        }

        VerticalScrollDecorator {
            flickable: chat
        }

        onCountChanged: {
            if (!chat.currentItem) {
                return;
            }

            // when previous last item is fully visible scroll to end
            var isLastVisible = (chat.currentItem.y + chat.currentItem.height) >= chat.contentY
                && (chat.currentItem.y + chat.currentItem.height) <= (chat.contentY + height)
            if(chat.currentIndex == 0 || isLastVisible) {
                var newIndex = chat.count - 1;
                chat.positionViewAtEnd();
                chat.currentIndex = newIndex;
            }
        }
    }

    Column {
        id: sendMessagePart
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        width: parent.width

        Row {
            width: parent.width
            visible: replyToId != -1
            spacing: Theme.paddingSmall
            height: Theme.iconSizeSmall

            Separator {
                width: Theme.horizontalPageMargin
            }

            Icon {
                id: replyIndicator
                source: "image://theme/icon-s-repost"
                color: palette.secondaryHighlightColor
            }

            Label {
                width: parent.width - Theme.horizontalPageMargin * 2 - Theme.iconSizeSmall - Theme.paddingSmall * 3 - replyToClear.width
                id: replyTo
                text: replyToMsg
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: "NoWrap"
                elide: "ElideMiddle"
                color: Theme.secondaryHighlightColor
                height: parent.height
            }
            IconButton {
                height: Theme.iconSizeSmall
                width: Theme.iconSizeSmall
                id: replyToClear
                icon.source: "image://theme/icon-s-clear-opaque-cross"
                icon.color: palette.secondaryHighlightColor
                onClicked: {
                    replyToMsg = ""
                    replyToId = -1
                }
            }
        }

        Row {
            width: parent.width

            TextArea {
                id: sendMessage
                width: parent.width - pickAttachment.width - sendIcon.width
                placeholderText: "Write something excellent"
                wrapMode: TextEdit.WordWrap
                EnterKey.enabled: text.length > 0
            }
            IconButton {
                id: pickAttachment
                anchors.top: sendMessage.top
                anchors.bottom: sendMessage.bottom
                visible: hasAttachments
                width: visible ? sendIcon.width : 0
                icon.source: "image://theme/icon-m-attach?" + sendMessage.color
                onClicked: {
                    room.preventClearing = true
                    pageStack.push(contentPickerPage)
                }
            }
            IconButton {
                id: sendIcon
                anchors.top: sendMessage.top
                anchors.bottom: sendMessage.bottom
                icon.source: "image://theme/icon-m-send?" + sendMessage.color
                Behavior on icon.source { FadeAnimation {} }
                opacity: sendMessage.text.length > 0 ? 1.0 : 0.3
                Behavior on opacity { FadeAnimation {} }
                onClicked: {
                    roomService.sendMessage(sendMessage.text, replyToId);
                    // FIXME: only clear text after it was send
                    sendMessage.text = ""
                    replyToId = -1
                }
            }
        }
    }

    RoomService {
        id: roomService
    }

    Connections {
        target: roomService
        onNewMessage: {
            message = prepareMessage(JSON.parse(message))
            if (message.referenceId && room.pendingMessages.hasOwnProperty(message.referenceId)) {
                messages.remove(room.pendingMessages[message.referenceId], 1)
            }
            updateLastOfActor(message)
            updateFirstOfActor(message)
            filterParameters(message)
            messages.append(message)
        }
    }

    Component {
        id: contentPickerPage
        ContentPickerPage {
            onSelectedContentPropertiesChanged: {
                var transferId = UploadService.uploadFile(selectedContentProperties.filePath, token, accountId)
                if (transferId !== "") {
                    var pendingMsg = prepareUploadMessage(
                                selectedContentProperties.filePath.split(/[/]/).pop(),
                                selectedContentProperties.filePath,
                                selectedContentProperties.mimeType,
                                transferId
                    )
                    appendPendingUploadMessage(pendingMsg)
                }
            }
        }
    }
}
