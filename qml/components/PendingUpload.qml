import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.nextcloud.talk 1.0

Row {
    property string account;
    property string fileId;
    property string filePath;
    property int size: Theme.itemSizeLarge
    property string transferReference;
    property bool isImage: true;

    height: size + errorText.height

    Item {
        id: imageContainer
        width: parent.width

        Rectangle {
            id: progressUnderlay
            color: Theme.highlightColor
            width: parent.width
            height: size
            visible: !isImage
            radius: height * 0.2
        }

        Image {
            id: image
            source: isImage ? "file://" + filePath : ""
            sourceSize: width + "x" + size
            anchors {
                top: progressUnderlay.top
                left: progressUnderlay.left
                right: progressUnderlay.right
            }

            fillMode: Image.PreserveAspectCrop
            visible: isImage
        }

        Rectangle {
            id: uploadProgress
            anchors {
                top: progressUnderlay.top
                right: progressUnderlay.right
                bottom: isImage ? image.bottom : progressUnderlay.bottom
            }
            width: progressUnderlay.width;
            opacity: 0.6
            color: "#000000"
            radius: isImage ? 0 : progressUnderlay.radius
        }

        BusyIndicator {
            size: BusyIndicatorSize.Large
            anchors.centerIn: image
            running: image.status === Image.Loading
        }

        Label {
            id: errorText
            anchors {
                top: isImage ? image.bottom : progressUnderlay.bottom
                right: isImage ? image.right : progressUnderlay.right
            }
            text: qsTr("Upload failed. No automatic retry.")
            visible: false;
        }
    }

    Connections {
        target: UploadService
        onUploadProgress: {
            if (transferId !== transferReference) {
                return;
            }
            uploadProgress.width = progressUnderlay.width * (100 - relProgress) / 100
        }
        onUploadFailed: {
            if (transferId !== transferReference) {
                uploadProgress.opacity = 0.3;
                errorText.visible = true
            }
        }
    }
}
