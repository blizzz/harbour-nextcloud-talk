import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.nextcloud.talk 1.0

Row {
    property string account;
    property string fileId;
    property string filePath;
    property string mimeType;
    property int size: Theme.itemSizeLarge
    property bool isDownloading: false
    property bool previewAvailable: true
    readonly property string downloadReference: (Math.random() * 10000).toString()

    function clickHandler() {
        if(DownloadService.fileExists(filePath, account)) {
            openFile()
        } else {
            isDownloading = true
            DownloadService.getFile(filePath, account, downloadReference)
        }
    }

    function openFile() {
        Qt.openUrlExternally(DownloadService.filePath(filePath, account))
    }

    height: size

    onPreviewAvailableChanged: {
        if (!previewAvailable) {
            image.fillMode = Image.PreserveAspectFit
            image.sourceSize = size + "x" + size

            var mime = mimeType.split('/')
            var edition = Theme.colorScheme === Theme.LightOnDark ? 'dark' : 'light'

            switch (mime[0]) {
                case "application":
                    switch (mime[1]) {
                        case "javascript":
                            image.source = "image://theme/icon-m-browser-javascript"
                            return
                        case "json":
                        case "x-perl":
                        case "x-php":
                        case "x-xml":
                        case "x-yaml":
                            image.source = "image://theme/icon-m-developer-mode"
                            return
                        case "msword":
                        case "vnd.lotus-wordpro":
                        case "vnd.ms-word":
                        case "vnd.oasis.opendocument.text":
                        case "vnd.openxmlformats-officedocument.wordprocessingml.document":
                        case "vnd.wordperfect":
                        case "x-iwork-pages-sffpages":
                            image.source = "image://theme/icon-m-file-formatted-" + edition
                            return
                        case "pdf":
                            image.source = "image://theme/icon-m-file-pdf-" + edition
                            return
                        case "vnd.android.package-archive":
                            image.source = "image://theme/icon-m-file-apk"
                            return
                        case "vnd.ms-excel":
                        case "vnd.oasis.opendocument.spreadsheet":
                        case "vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                        case "x-iwork-numbers-sffnumbers":
                            image.source = "image://theme/icon-m-file-spreadsheet-" + edition
                            return
                        case "vnd.ms-powerpoint":
                        case "vnd.oasis.opendocument.presentation":
                        case "vnd.openxmlformats-officedocument.presentationml.presentation":
                        case "vnd.openxmlformats-officedocument.presentationml.slideshow":
                        case "x-iwork-keynote-sffkey":
                            image.source = "image://theme/icon-m-file-presentation-" + edition
                            return
                        case "x-7z-compressed":
                        case "x-bzip2":
                        case "x-compressed":
                        case "x-gzip":
                        case "x-rar-compressed":
                        case "x-tar":
                        case "zip":
                            image.source = "image://theme/icon-m-file-archive-folder"
                            return
                        default:
                            image.source = "image://theme/icon-m-file-other-" + edition
                            return
                    }
                case "audio":
                    image.source = "image://theme/icon-m-file-audio"
                    return
                case "image":
                    image.source = "image://theme/icon-m-file-image"
                    return
                case "text":
                    image.source = "image://theme/icon-m-file-document-" + edition
                    return
                case "font":
                    image.source = "image://theme/icon-m-font-size"
                    return
                case "video":
                    image.source = "image://theme/icon-m-file-video"
                    return
                default:
                    image.source = "image://theme/icon-m-file-other-" + edition
            }
        }
    }

    Item {
        id: imageContainer
        width: parent.width
        height: size

        Image {
            id: image
            source: (account > 0 && previewAvailable) ? "image://preview/" + account + "/" + fileId + "/" : ""
            sourceSize: width + "x" + size
            height: size
            width: parent.width
            fillMode: Image.PreserveAspectCrop
            horizontalAlignment: Image.AlignLeft

            MouseArea {
                anchors.fill: parent;
                propagateComposedEvents: true
                onClicked: {
                    mouse.accepted = true
                    clickHandler()
                }
            }

            onStatusChanged: {
                if (status === Image.Error) {
                    previewAvailable = false
                }
            }
        }

        BusyIndicator {
            size: parent.height > Theme.itemSizeMedium ? BusyIndicatorSize.Large : BusyIndicatorSize.Medium
            anchors.centerIn: image
            running: image.status === Image.Loading || isDownloading
        }
    }

    Connections {
        target: DownloadService
        onFileDownloaded: {
            if (downloadID === downloadReference) {
                isDownloading = false
                openFile()
            }
        }
    }
}
