import QtQuick 2.0
import Sailfish.Silica 1.0

Label {
    property bool firstActorOfGroup: true
    property bool isPending: false
    property string author
    property int time

    readonly property var time_format: ({hour: '2-digit', minute: '2-digit'})
    readonly property var date_format: ({day: '2-digit', motnh: '2-digit'})

    text: {
        if (isPending) {
            return qsTr("Pending…")
        }

        if (firstActorOfGroup) {
            return getLocalizedTime() + " · " + author + " · " + getLocalizedDate()
        }
        return getLocalizedTime()
    }
    textFormat: Text.PlainText;
    anchors {
        left: parent.left
        right: parent.right
    }
    font.pixelSize: Theme.fontSizeTiny
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere

    function getLocalizedTime() {
        return new Date(time * 1000).toLocaleTimeString(undefined, time_format)
    }

    function getLocalizedDate() {
        return new Date(time * 1000).toLocaleDateString(undefined, date_format)
    }
}


