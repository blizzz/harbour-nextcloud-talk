import QtQuick 2.0
import Sailfish.Silica 1.0
import "../../lib/NcChatTools.js" as ChatTools

Item {
    id: root

    property string rawMessage
    property var parameters
    property string author
    property string userId
    property string roomId
    property int accountId

    readonly property string messageStyleSheet:
        "<style>" +
            "a:link { color: " + Theme.highlightColor + "; }" +
            ".highlight { color: " + Theme.highlightColor + "; }" +
        "</style>";

    height: messageText.height

    LinkedLabel {
        id: parser
        visible: false

        onTextChanged: {
            var message = ChatTools.handleMessageParameters(root.parameters, parser.text, userId, roomId)
            if (message === "{file}") {
                var actorSnippet = ChatTools.createMentionSnippet(parameters['actor'], userId, roomId);
                var path = parameters['file'].path

                message = actorSnippet + " " + qsTr("shared") + " " +
                        '<a rel="noopener noreferrer" ' +
                        'href="javascript:DownloadService.getFile(\"' + path + "\", " + root.accountId + ')">' +
                        parameters['file'].name + '</a>';
            }
            messageText.text = root.messageStyleSheet + message
        }
    }

    Label {
        id: messageText
        anchors {
            right: parent.right
            left: parent.left
        }
        textFormat: Text.RichText
        height: contentHeight
        font.pixelSize: Theme.fontSizeSmall
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        onLinkActivated: Qt.openUrlExternally(link)
    }

    onRawMessageChanged: {
        var message = root.rawMessage.replace('{actor}', author)

        parser.plainText = message
    }
}
