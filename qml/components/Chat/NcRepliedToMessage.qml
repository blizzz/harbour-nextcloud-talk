import QtQuick 2.0
import Sailfish.Silica 1.0

Row {
    property string author
    property string message

    visible: repliedTo.message !== ""
    anchors {
        left: parent.left
        right: parent.right
    }

    Rectangle {
        id: repliedToIndicator
        color: Theme.secondaryColor
        width: 2
        height: repliedToAuthor.height + repliedToText.height
    }

    Column {
        width: parent.width - repliedToIndicator.width

        Label {
            id: repliedToAuthor
            width: parent.width
            text: author
            textFormat: Text.PlainText
            anchors {
                left: parent.left
                right: parent.right
            }
            leftPadding: Theme.paddingSmall
            font.pixelSize: Theme.fontSizeExtraSmall
            wrapMode: Text.NoWrap
            elide: Text.ElideMiddle
            visible: author !== ""
            color: Theme.secondaryColor
            font.italic: true
            height: visible ? contentHeight : 0
        }
        Item {
            anchors {
                left: parent.left
                right: parent.right
            }
            height: repliedToText.height

            Label {
                id: repliedToText
                text: message
                textFormat: Text.PlainText
                anchors {
                    left: parent.left
                    right: parent.right
                }
                leftPadding: Theme.paddingSmall
                font.pixelSize: Theme.fontSizeExtraSmall
                wrapMode: Text.NoWrap
                elide: Text.ElideMiddle
                visible: message !== ""
                color: Theme.secondaryColor
                font.italic: true
                height: visible ? contentHeight : 0
            }

            MouseArea {
                anchors.fill: repliedToText
                onClicked: {
                    if (repliedToText.elide == Text.ElideMiddle) {
                        repliedToAuthor.elide = Text.ElideNone
                        repliedToAuthor.wrapMode = Text.WrapAtWordBoundaryOrAnywhere

                        repliedToText.elide = Text.ElideNone
                        repliedToText.wrapMode = Text.WrapAtWordBoundaryOrAnywhere
                    } else {
                        repliedToAuthor.elide = Text.ElideMiddle
                        repliedToAuthor.wrapMode = Text.NoWrap

                        repliedToText.elide = Text.ElideMiddle
                        repliedToText.wrapMode = Text.NoWrap
                    }
                }
            }
        }
    }
}
