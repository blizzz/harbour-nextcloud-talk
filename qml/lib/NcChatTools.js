.pragma library

var TEMPLATE_MENTION = "<strong class='{CLASS}'>{MENTION}</strong>";

function handleMessageParameters(parameters, message, userId, token) {
    Object.keys(parameters).forEach(function(key) {
        // system message "You added {user}"
        if(key === 'user') {
            message = message.replace('{' + key + '}', parameters[key].name)
        }
        if(key.substring(0, 8) === 'mention-') {
            var insertSnippet = createMentionSnippet(parameters[key], userId, token);
            message = message.replace('{' + key + '}', insertSnippet)
        }
    })
    return message
}

function createMentionSnippet(messageParameters, userId, token) {
    var useClass = ''
    if(messageParameters.id === userId) {
        useClass = 'highlight'
    } else if(messageParameters.id === token
              && messageParameters.type === 'call') {
        useClass = 'highlight'
    }
    var mentionSnippet = TEMPLATE_MENTION.replace('{CLASS}', useClass)

    return mentionSnippet.replace('{MENTION}', messageParameters.name)
}
